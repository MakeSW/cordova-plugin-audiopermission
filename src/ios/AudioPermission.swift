import AVFAudio

@objc(AudioPermission) class AudioPermission : CDVPlugin {
    @objc(checkPermission:)
    func checkPermission(command: CDVInvokedUrlCommand) {
        AVAudioSession.sharedInstance().requestRecordPermission { (granted: Bool) -> Void in
            var pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
            
            if granted {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: true)
            } else {
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: false)
            }

            self.commandDelegate!.send(pluginResult, callbackId: command.callbackId)
        }
    }
}
