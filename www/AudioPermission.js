var exec = require('cordova/exec');

exports.checkPermission = function () {
    return new Promise((resolve, reject) => {
        exec(
            resolve,
            reject,
            'AudioPermission',
            'checkPermission',
        );
    });
};
